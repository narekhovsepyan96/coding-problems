#include<iostream>
#include<unordered_map>

bool check_permutation(const std::string& a,const std::string& b) {
	std::unordered_map<char,int> m;
	

	for(std::size_t i{};i!=a.size();++i)
		++m[a[i]];

	for(const auto& i:b)
		--m[i];

	for(const auto& i:m)
		if(i.second != 0)
			return false;


	return true;
}




int main() {
	std::cout << std::boolalpha 
		      << check_permutation("narekk","narekk")
			  << std::endl
			  << check_permutation("narek","narekk");
	
	return {};
}
