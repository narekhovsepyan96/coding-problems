#include<iostream>
#include<string>

std::string URLify(const std::string& in) {
	std::string tmp;

	for(const auto& i:in)
		if(i == ' ')
			tmp += "0x20";
		else tmp += i;

	return tmp;
}

int main() {

	std::cout << URLify("narek hovsepian")
		      << std::endl;

	return{};
}
