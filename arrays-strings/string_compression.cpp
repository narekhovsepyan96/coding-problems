#include<iostream>
#include<string>
#include<unordered_map>


std::string string_compression(const std::string& in) {
	
	std::unordered_map<char,int> m;	
	std::string out;
	for(const auto& i:in)
		++m[i];
	
	for(const auto& i: in)
		if(m[i] != 0)
			out += i, out += std::to_string(m[i]), m[i] = 0;

	return out;
}




int main() {

	std::cout << string_compression("aaaabbbbccccck")
			  << std::endl;

	return{};
}
