
#include<iostream>
#include<string>


bool is_unique(const std::string& in) {
	for(std::size_t i {}; i!= in.size();++i) {
		for(std::size_t j{};j!=in.size();++j) {
			if(i != j && in[i] == in[j])
				return false;
		}
	}
	return true;
}


int main() {

	std::cout << std::boolalpha
			  << is_unique("narek n")
			  << std::endl
			  << is_unique("narek") 
			  << std::endl;

	return{};
}
