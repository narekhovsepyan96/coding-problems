#include<iostream>
#include<vector>



int main() {

	auto cool_zero = [&](std::vector<std::vector<int>>& in, std::size_t cool) -> void {
		for(std::size_t i{};i!=in.size();++i)
			in[i][cool] = 0;
	};

	auto row_zero = [&](std::vector<std::vector<int>>& in, std::size_t row) -> void {
		for(std::size_t i{};i!=in[row].size();++i)
			in[row][i] = 0;
	};

	std::vector<std::vector<int>> v{
		{1,2,3,4},
		{5,6,7,8},
		{9,10,11,12},
		{13,14,0,16}
	};
	bool t = false;
	for(std::size_t i{};i!=v.size();++i){
		for(std::size_t j{};j!=v[i].size();++j){
			if(v[i][j] == 0)
				t = true,
				  cool_zero(v,j);				
		}
		if(t)
			t = false,
			row_zero(v,i);
	}
	
	for(const auto& i:v) {
		for(const auto& j:i)
			std::cout << j << " ";
		std::cout << std::endl;
	}

	return{};
}
