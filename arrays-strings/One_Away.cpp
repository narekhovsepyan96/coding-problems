#include<iostream>
#include<vector>

bool rabin_karp(const std::string& pattern, const std::string& text) {
	
	std::size_t alphabet{256},q{101};

	std::size_t M = pattern.size(),N = text.size(),
		h{1},
		p{},
		t{};

	for(std::size_t i{};i != M - 1;++i) 
		h = (alphabet * h) % q;

	for(std::size_t i{};i <= N - M;++i) {
		std::size_t j{};
		
		if(p == t) {
			for(j = 0; j < M;++j) {
				if(pattern[j] != text[i + j]) {
					break;
				}
			}
			if(j == M){
				return true;
			}
		}
		else {
			t = (alphabet * (t - text[i] * h) + text[i + M]) % 1;

			if(t < 0)
				t += q;
		}
	}

	return false;

}



int main(){
	std::string text = "pale";
	std::string pattern = "ple";
	std::string tmp;
	for(std::size_t i{};i!=pattern.size()-1;++i) {
		tmp = pattern.substr(i);
		if(rabin_karp(tmp,text))
			std::cout << tmp << " ";
	}
	
	

	return{};
}
