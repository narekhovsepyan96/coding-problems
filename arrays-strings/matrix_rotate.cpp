#include<iostream>
#include<vector>




int main(){
	
	std::vector<std::vector<int>> v{
		{1,2,3,4},
		{5,6,7,8},
		{9,10,11,12},
		{13,14,15,16}
	};
	
	std::vector<std::vector<int>> out(v.size(),std::vector<int>(v[0].size()));

	for(std::size_t i{};i!= v.size();++i) {
		for(std::size_t j{}; j!= v[i].size();++j){
			out[j][i] = v[v.size() - i - 1][j];
		}
	}





	for(const auto& i : out){
		for(const auto& j : i)
			std::cout << j << " ";
		std::cout << std::endl;
	}


	return{};
}
