#include<iostream>
#include<list>


int main() {
	
	std::list<int> l{1,2,3,4,5,6};

	std::list<int>::iterator slow{l.begin()};
	std::list<int>::iterator fast{l.begin()};

	while(fast != l.end()) {
		++fast;
		++fast;
		++slow;
	}
	
	l.remove(*slow);

	for(const auto& i:l)
		std::cout << i << " ";

	return{};
}
