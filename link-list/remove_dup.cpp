#include<iostream>
#include<list>
#include<unordered_map>

int main() {
	std::unordered_map<int,std::size_t> mp;

	std::list<int> l{1,2,3,4,6,1,2,3,6,6};

	for(const auto& i:l)
		++mp[i];

	for(const auto& i:mp){
		if(i.second > 1)
			l.remove(i.first),
			l.push_back(i.first);
	}

	for(const auto& i: l)
		std::cout << i << " ";

	return {};
}
