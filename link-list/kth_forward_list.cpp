#include<iostream>
#include<forward_list>




int main(){
	
	std::forward_list<int> fl{1,2,3,4,5,6,7,8,9,10,11};
	
	std::forward_list<int>::iterator iter {fl.begin()};
	
	std::size_t k{5};
	

	while(k-- != 0){
		++iter;
	}

	std::cout << *iter << std::endl;
	return{};
}
