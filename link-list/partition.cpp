#include<iostream>
#include<vector>
#include<algorithm>


template<class _Iter,class _Pred>
_Iter partition(_Iter first, _Iter last, _Pred p){
	first = std::find_if_not(first,last,p);

	if(first == last) return first;

	for(auto i = std::next(first);i!=last;++i) {
		if(p(*i)) {
			std::iter_swap(i,first);
			++first;
		}
	}
	return first;
}






int main(){
	std::vector<int> v{1,3,4,4,56,6,7,5};
	
	int x = 5;
	::partition(v.begin(),v.end(),[&](auto l){
				return l > x;
			});
	
	for(const auto& i:v)
		std::cout << i << " ";
	return{};
}
