#include<iostream>
#include<list>



int main() {
	
	auto number = [&](std::list<int>& l) -> int {
		int tmp{};
		int k{1};
		for(const auto& i:l)
			tmp += i * k,
			k *= 10;

		return tmp;
	};


	std::list l1 {7,1,6};
	std::list l2 {5,9,2};

	std::cout << number(l1) + number(l2)<< std::endl;	
	

	return{};
}
