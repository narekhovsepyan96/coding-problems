#include<iostream>

struct ListNode{
	int val;
	ListNode* next;
	ListNode(const int& item):val{item},next{nullptr} {}
};


void destroy(ListNode* in){
	ListNode* tmp{nullptr};
	while(in){
		tmp = in;
		in = in->next;
		delete tmp;
	}
}

ListNode* iterative_detectCycle(ListNode* head){
	ListNode *p = head, *q = head;
	while(1){
		if(p->next)
			p = p->next;
		else return nullptr;
		if(q->next && q->next->next)
			q = q->next->next;
		else return nullptr;
		if(p == q){
			p = head;
			while(p!=q){
				p = p->next;
				q = q->next;
			}
			return p;
		}
	}
}


int main(){
	ListNode* head = new ListNode{3};
	head->next = new ListNode{2};
	head->next->next = new ListNode{0};
	head->next->next->next = new ListNode{-4};
	
	head->next->next->next->next = head->next;
	
	ListNode* tmp; //= iterative_detectCycle(head);
	tmp = iterative_detectCycle(head);

	std::cout << tmp->val << "\n";

	head->next->next->next->next = nullptr;
	destroy(head);
	return{};
}




